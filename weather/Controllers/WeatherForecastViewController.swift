//
//  WeatherForecastViewController.swift
//  weather
//
//  Created by Yuriy Berdnikov on 9/19/17.
//  Copyright © 2017 perpet.io. All rights reserved.
//

import UIKit
import CoreLocation
import ForecastIO
import SVProgressHUD

class WeatherForecastViewController: UIViewController {
    
    @IBOutlet fileprivate weak var contentTextView: UITextView?
    
    var coordinate: CLLocationCoordinate2D?
    fileprivate let forecastToken = "4fdd820372de3bede2ddf5724694bbbe"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadForecast()
    }
}

fileprivate extension WeatherForecastViewController {
    func loadForecast() {
        guard let coordinate = self.coordinate else {
            return
        }
        
        SVProgressHUD.show()
        
        let client = DarkSkyClient(apiKey: forecastToken)
        client.units = .si
        client.getForecast(latitude: coordinate.latitude, longitude: coordinate.longitude) {[weak self] result in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                
                switch result {
                case .success(let currentForecast, _):
                    self?.populateViewWithForecastData(forecast: currentForecast)
                    break
                    
                case .failure(let error):
                    self?.present(UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert), animated: true, completion: nil)
                    break
                }
            }
        }
    }
    
    func populateViewWithForecastData(forecast: Forecast) {
        guard let currentForecast = forecast.currently else {
            return
        }
        
        var forecastData = [String]()
        
        if let summary = currentForecast.summary {
            forecastData.append(summary)
        }
        
        if let temperature = currentForecast.temperature {
            forecastData.append("Temperature: \(temperature) C")
        }
        
        if let windSpeed = currentForecast.windSpeed {
            forecastData.append("Wind Speed: \(windSpeed) miles per hour")
        }
        
        if let humidity = currentForecast.humidity {
            forecastData.append("Humidity: \(humidity * 100) %")
        }
        
        if let pressure = currentForecast.pressure {
            forecastData.append("Pressure: \(pressure) millibars")
        }

        if let visibility = currentForecast.visibility {
            forecastData.append("Visibility: \(visibility) miles")
        }
        
        if let ozone = currentForecast.ozone {
            forecastData.append("Ozone: \(ozone) Dobson units")
        }

        self.contentTextView?.text = forecastData.joined(separator: "\n")
    }
}
